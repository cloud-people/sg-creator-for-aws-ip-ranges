# Automatic SG maintainer for AWS IP ranges

This tool uses the [ip-ranges.json](https://ip-ranges.amazonaws.com/ip-ranges.json) and [the public AWS SNS topic](https://docs.aws.amazon.com/general/latest/gr/aws-ip-ranges.html#subscribe-notifications) for ip range creation and updates.

It needs access to your VPC SG configuration therefore we need extra IAM roles outside of just running the lambda, I tried to keep these IAM permissions as minimal as possible. To check out what iam permissions are set please review the template above or in the git repo.

## Services to choose from
More info about these can be found [here](https://docs.aws.amazon.com/general/latest/gr/aws-ip-ranges.html)
- AMAZON
- AMAZON_APPFLOW
- AMAZON_CONNECT
- API_GATEWAY
- CHIME_MEETINGS
- CHIME_VOICECONNECTOR
- CLOUD9
- **CLOUDFRONT** `default`
- CODEBUILD
- DYNAMODB
- EC2
- EC2_INSTANCE_CONNECT
- GLOBALACCELERATOR
- KINESIS_VIDEO_STREAMS
- ROUTE53
- ROUTE53_HEALTHCHECKS
- S3
- WORKSPACES_GATEWAYS


To test locally use:
```
sam local invoke "SgUpdateFunction" -e event.json
```
you might need to update the md5hash in the event to match the real ip-ranges.json

The lambda function used here can be found at: s3://awsiammedia/public/sample/406_Automatically_Update_Security_Groups/lambda.py
I only added support for a trigger by cloudformation to make the SG initiate on creation of the stack.


# [Launch in AWS](https://console.aws.amazon.com/lambda/home#/create/app?applicationId=arn:aws:serverlessrepo:eu-west-1:456407058254:applications/sg-creator-for-aws-ip-ranges)

## Made with ❤️ by [Cloud People](https://www.linkedin.com/company/hcs-cloud-people/)
[![Cloud People Logo](https://assets.gitlab-static.net/uploads/-/system/group/avatar/7133597/cloud_people_icon.png?width=64)](https://www.linkedin.com/company/hcs-cloud-people/)